<?php 

	$args = array(  
        'post_type' => 'automobiles',
        'post_status' => 'publish',
        'posts_per_page' => 4, 
        'orderby' => 'title', 
        'order' => 'ASC',
        'cat' => 'cars',
    );
   	
	$getPost = new wp_query($args);
	global $post;
	if($getPost->have_posts()){
		echo '<ul>';
			while ( $getPost->have_posts()):$getPost->the_post();

				echo "<li><h2>".$post->post_title."</h2></li>";
				
			endwhile;
		echo '</ul>';
	}

    wp_reset_postdata(); 
?>